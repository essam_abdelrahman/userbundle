<?php
namespace DepartmentOne\CoreBundle\EventListener;

use DepartmentOne\CoreBundle\Controller\UserDataController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use DepartmentOne\CoreBundle\Services\User;

class UserDataListener
{
    private $logger;
    private $session;
    private $manipulateTime = false;
    private $currentTime    = "";

    public function __construct( $logger, $session, $manipulate_time, $my_time )
    {
        $this->logger           = $logger;
        $this->session          = $session;
        $this->manipulateTime   = $manipulate_time;
        $this->currentTime      = $my_time;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof UserDataController) {
            $user = new User( $this->logger, $this->session, $this->manipulateTime, $this->currentTime );
        }
    }
}