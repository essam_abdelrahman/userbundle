/**
* Dept1BaseJSFile
 * - contains custom scripts
 */

$("#loading").click(function(){
    $(this).hide();
});

$(document).ready( function() {

    // enables link in labels (instead of selecting checkboxes or something)
    $(document).on("tap click", 'label a', function( event, data ){
        event.stopPropagation();
        event.preventDefault();
        window.open($(this).attr('href'), $(this).attr('target'));
        return false;
    });

    // Add custom validation rule
    $.formUtils.addValidator({
        name : 'ageCheck',
        validatorFunction : function(value, $el, config, language, $form) {
            var year = $('#year').val();
            var month = ( $('#month').val() < 10 ) ? '0' + $('#month').val() : $('#month').val();
            var day = ( $('#day').val() < 10 ) ? '0' + $('#day').val() : $('#day').val();
            var birthday = year + month + day;
            if ('{{ project.yesterBirthDay }}' < birthday ) {
                return false;
            } else {
                return true;
            }
        },
        errorMessage : 'Du bist leider noch nicht alt genug.',
        errorMessageKey: 'badAge'
    });

    $.formUtils.addValidator({
        name : 'signSet',
        validatorFunction : function(value, $el, config, language, $form) {
            // each sign has to be from this set
            // /^[a-zA-Z0-9$%\&\/\(\)\+\-\._]+$/
            // Es sind nur Buchstaben, Ziffern und diese Sonderzeichen erlaubt: $ % & / ( ) + - . _ -
            if (
                    /\d/.test(value)        &&  // at least one digit
                    /[A-Z]/.test(value)     &&  // at least one big alpha
                    /[a-z]/.test(value)     &&  // at least on small alpha
                    /.{8,}/.test(value)         // at least 8 signs
               ) {
                // TODO: bad place for that
                $(".password_hint").removeClass('orange');
                return true;
            } else {
                // TODO: bad place for that
                $(".password_hint").addClass('orange');
                return false;
            }
        },
        errorMessage : 'Dein Passwort entspricht nicht den Anforderungen.',
        errorMessageKey: 'badSignSet'
    });

    $.formUtils.addValidator({
        name : 'dayCheck',
        validatorFunction : function(value, $el, config, language, $form) {
            if (
                    /\d/.test(value) && 0 < value && value < 32
               ) {
                return true;
            } else {
                console.log('validation error');
                return false;
            }
        },
        errorMessage : 'Fehler im Tag gefunden.',
        errorMessageKey: 'badDayCheck'
    });

    $.formUtils.addValidator({
        name : 'monthCheck',
        validatorFunction : function(value, $el, config, language, $form) {
            if (
                    /\d/.test(value) && 0 < value && value < 13
               ) {
                return true;
            } else {
                console.log('validation error');
                return false;
            }
        },
        errorMessage : 'Fehler im Monat gefunden.',
        errorMessageKey: 'badDayCheck'
    });

    $.formUtils.addValidator({
        name : 'yearCheck',
        validatorFunction : function(value, $el, config, language, $form) {
            var currentYear = 2016;
            var maxAge = 125;
            if (
                    /\d/.test(value) && currentYear-maxAge < value && value < currentYear+1
               ) {
                return true;
            } else {
                console.log('validation error');
                return false;
            }
        },
        errorMessage : 'Fehler im Jahr gefunden.',
        errorMessageKey: 'badDayCheck'
    });
});

/*****************************************************************************
 * Basic JS-Functions
 *****************************************************************************/


