// adjusted error messages

/**
 * jQuery Form Validator
 * ------------------------------------------
 *
 * German language package
 *
 * @website http://formvalidator.net/
 * @license MIT
 */
(function($, window) {

  'use strict';

  $(window).bind('validatorsLoaded', function() {

    $.formUtils.LANG = {
      andSpaces: 						' und Leerzeichen',
      badAlphaNumeric: 					'Sie können nur alphanumerische Zeichen (Buchstaben und Zahlen) eingeben',
      badAlphaNumericExtra: 			' und',
      badBrazilCEPAnswer: 				'Der CEP ist ungültig',
      badBrazilCPFAnswer: 				'Der CEP ist ungültig',

      badBrazilTelephoneAnswer: 		'Die eingegebene Telefonnummer ist nicht korrekt',
      badCVV: 							'Sie haben eine falsche CVV eingegeben',
      badCreditCard: 					'Sie haben eine ungültige Kreditkartennummer eingegeben',
      badCustomVal: 					'Eingabe einer falschen Antwort',
      badDate: 							'Eingabe eines falschen Datums',

      badDomain: 						'Sie haben die falsche Domäne eingetragen',
      badEmail: 						'Das ist keine gültige E-Mail-Adresse.',
      badInt: 							'Sie haben keine Nummer eingegeben',
      badNumberOfSelectedOptionsEnd: 	' Antwort',
      badNumberOfSelectedOptionsStart: 	'Wählen Sie zu mindestens ',

      badSecurityAnswer: 				'Sie haben die falsche Antwort auf die Sicherheitsfrage eingegeben',
      badSecurityNumber: 				'Sie haben eine falsche Sozialversicherungsnummer eingegeben',
      badStrength: 						'Sie haben ein Kennwort, das nicht sicher genug ist eingegeben',
      badTelephone: 					'Sie haben keine richtige Telefonnummer eingetragen',
      badTime: 							'Sie haben nicht die korrekte Zeit eingegeben',

      badUKVatAnswer: 					'Sie haben keine UK-Umsatzsteuer-Identifikationsnummer eingegeben',
      badUrl: 							'Sie haben nicht die richtige URL eingegeben',
      errorTitle: 						'Ihre Anfrage konnte nicht gesendet werden!',
      groupCheckedEnd: 					' Auswahl',
      groupCheckedRangeStart: 			'Wählen Sie zwischen',
      groupCheckedTooFewStart: 			'Dann müssen Sie zumindest sicher,',

      groupCheckedTooManyStart: 		'Sie können nicht mehr als zu machen',
      imageRatioNotAccepted : 			'Bildverhältnis wird nicht akzeptiert',
      imageTooSmall: 					'Bild ist zu klein',
      imageTooTall: 					'Bild kann nicht größer sein als',
      imageTooWide: 					'Bild kann nicht breiter sein als',
      lengthBadEnd: 					'  Zeichen',

      lengthBadStart: 					'Wir benötigen eine Eingabe zwischen ',
      lengthTooLongStart: 				'Die Eingabe erlaubt höchstens ',
      lengthTooShortStart: 				'Die Eingabe erfordert mindestens ',
      max: 								'max',
      min: 								'min',

      notConfirmed: 					'Die Antworten könnten nicht gegenseitig bestätigen,',
      requiredField: 					'Das ist ein Pflichtfeld.',
      requiredFields: 					'Sie haben nicht alle Fragen beantwortet',
      wrongFileDim: 					'Illegale Bildgröße,',
      wrongFileSize: 					'Die Datei, die Sie hochzuladen versuchen, ist zu groß (max %s)',
      wrongFileType: 					'Nur Dateien vom Typ %s sind zulässig'
    };

  });

})(jQuery, window);