<?php
namespace DepartmentOne\CoreBundle\Resources\config;

class ValidationSettings
{
    // messages
    const GENDER_MALE                   = "MALE";
    const GENDER_FEMALE                 = "FEMALE";
    const GENDER_MESSAGE                = "Choose a valid gender.";

    const DEFAULT_MAX_INPUT_LENGHT      = 50;
    const DEFAULT_MAX_INPUT_MESSAGE     = "Die Eingabe ist auf 50 Zeichen begrenzt.";

    const SMALL_MAX_INPUT_LENGHT        = 25;
    const SMALL_MAX_INPUT_MESSAGE       = "Die Eingabe ist auf 25 Zeichen begrenzt.";

    const DATE_MAX_INPUT_LENGHT         = 10;
    const DATE_MAX_INPUT_MESSAGE        = "Die Eingabe ist auf 10 Zeichen begrenzt.";

    const DATE_MIN_INPUT_LENGHT         = 10;
    const DATE_MIN_INPUT_MESSAGE        = "Das Datum muss mindestens 10 Zeichen lang sein.";
    const DATE_EXACT_INPUT_MESSAGE      = "Das Datum muss genau 10 Zeichen lang sein (dd-mm-jjjj).";

    const CARD_MAX_INPUT_LENGHT         = 8;
    const CARD_MAX_INPUT_MESSAGE        = "Die Eingabe ist auf 8 Zeichen begrenzt.";

    const GENDER_MAX_INPUT_LENGHT       = 6;
    const GENDER_MAX_INPUT_MESSAGE      = "Die Eingabe ist auf 6 Zeichen begrenzt.";

    const PASSWORD_MIN_INPUT_LENGHT     = 6;
    const PASSWORD_MIN_INPUT_MESSAGE    = "Das Passwort muss mindestens 6 Zeichen lang sein.";

    const DEFAULT_NOT_BLANK_MESSAGE     = "Diese Angabe fehlt noch.";
    const DEFAULT_EMAIL_MESSAGE         = "Diese E-Mail-Adresse kann nicht verarbeitet werden.";
    const DEFAULT_TYPE_BOOL_MESSAGE     = "Der Wert darf nur ja oder nein betragen.";
}
