<?php

namespace DepartmentOne\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

//TODO CoreBundle Should be independent and never use a project classes.
use Dept1\McdBundle\RequestObjects\CancelMembershipData;
use Dept1\McdBundle\RequestObjects\ChangePasswordData;
use Dept1\McdBundle\RequestObjects\LoginData;
use Dept1\McdBundle\RequestObjects\NewCardData;
use Dept1\McdBundle\RequestObjects\NewEmailData;
use Dept1\McdBundle\RequestObjects\NewOptinData;
use Dept1\McdBundle\RequestObjects\NewProfileData;
use Dept1\McdBundle\RequestObjects\RegisterData;
use Dept1\McdBundle\RequestObjects\ResetPasswordData;
use Dept1\McdBundle\RequestObjects\RequestPasswordData;
use Dept1\McdBundle\RequestObjects\NewRaffleData;

class ApiController extends Controller
{
    public $debug               = false;               // or true to use dummy data;
    public $simBackEnd          = true;                // or false to just return (in debug mode) sended data;
    public $redirectOnErrorUrl  = "dept1_mcd_index";   // default page on error;
    public $CID                 = "";                  // customer id, set by request
    public $haveToCheckCaptcha  = false;               // or true to validate captcha;

    public function __construct()
    {
    }

/* ------------------------------------------------------------------------
 *      Actions
 * ----------------------------------------------------------------------*/

    /********************************
     *  test iqone api connection
     ***************************** */
    public function checkApiAction()
    {
        // send json response to caller
        return $this->callBackend();
    }

    /*********************************
     *  post register data
     ****************************** */
    public function postRegisterDataAction()
    {
        // set up backend call
        $csrfPhrase                 = "check_register_form";
        $dataObject                 = new RegisterData();        // init object that holds request data
        $type                       = "post";                    // set request type
        $resource                   = "customers";               // backend resource, see wiki
        $context                    = "customers";               // id to set the backend error code in an context
        $this->haveToCheckCaptcha   = true;                      // form has a captcha to be validated

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /*********************************
     *  post login data
     ****************************** */
    public function postLoginDataAction()
    {
        // set up backend call
        $csrfPhrase                 = "check_login_form";
        $dataObject                 = new LoginData();        // init object that holds request data
        $type                       = "post";                 // set request type
        $resource                   = "customers/login";      // backend resource, see wiki
        $context                    = "customers/login";      // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }

    /*********************************
     *  post password request data
     ****************************** */
    public function postPasswordRequestDataAction()
    {
        // set up backend call
        $csrfPhrase                 = "check_request_password_form";
        $dataObject                 = new RequestPasswordData();        // init object that holds request data
        $type                       = "post";                           // set request type
        $resource                   = "customers/passwordRequest";      // backend resource, see wiki
        $context                    = "customers/passwordRequest";      // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }

    /*********************************************
     *  post password reset data (link from email)
     ******************************************* */
    public function postPasswordResetDataAction()
    {
        // set up backend call
        $csrfPhrase                 = "check_reset_password_form";
        $dataObject                 = new ResetPasswordData();                              // init object that holds request data
        $type                       = "post";                                               // set request type
        $user                       = $this->get('dept1_basic.user');                       // load service
        $resource                   = "customers/passwordReset/" . $user->getAuthCode();    // backend resource, see wiki
        $context                    = "customers/passwordReset/{HASH}";                            // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }

    /***************************************
     *  post password to cancel membership
     ************************************* */
    public function postCancelMembershipAction()
    {
        // set up backend call
        $csrfPhrase     = "check_leave_form";
        $dataObject     = new CancelMembershipData();               // init object that holds request data
        $type           = "post";                                   // set request type
        $resource       = "customer/". $this->getCid() . "/cancel";   // backend resource, see wiki
        $context        = "customer/{CID}/cancel";                        // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /***************************************
     *  put new password
     ************************************* */
    public function putNewPasswordAction()
    {
        // set up backend call
        $csrfPhrase     = "check_change_password_form";
        $dataObject     = new ChangePasswordData();                 // init object that holds request data
        $type           = "put";                                    // set request type
        $resource       = "customer/" . $this->getCid() . "/password";     // backend resource, see wiki
        $context        = "customer/{CID}/password";                      // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /***************************************
     *  put new email
     ************************************* */
    public function putNewEmailAction()
    {
        // set up backend call
        $csrfPhrase     = "check_change_email_form";
        $dataObject     = new NewEmailData();                   // init object that holds request data
        $type           = "put";                                // set request type
        $resource       = "customer/" . $this->getCid() . "/email";    // backend resource, see wiki
        $context        = "customer/{CID}/email";                     // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /***************************************
     *  put new profile
     ************************************* */
    public function putNewUserAction()
    {
        // set up backend call
        $csrfPhrase     = "check_change_profile_form";
        $dataObject     = new NewProfileData();                 // init object that holds request data
        $type           = "put";                                // set request type
        $resource       = "customer/". $this->getCid();             // backend resource, see wiki
        $context        = "customer/{CID}";                   // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /***************************************
     *  put new optins
     ************************************* */
    public function putNewOptinAction()
    {
        // set up backend call
        $csrfPhrase     = "check_change_optin_form";
        $dataObject     = new NewOptinData();               // init object that holds request data
        $type           = "put";                            // set request type
        $resource       = "customer/" . $this->getCid() . "/optin";     // backend resource, see wiki
        $context        = "customer/{CID}/optin";                 // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }


    /*********************************
     *  post new card data
     ****************************** */
    public function postNewCardDataAction()
    {
        // set up backend call
        $csrfPhrase     = "check_add_card_form";
        $dataObject     = new NewCardData();                    // init object that holds request data
        $type           = "post";                               // set request type
        $resource       = "customer/" . $this->getCid() . "/loyaltyCard";  // backend resource, see wiki
        $context        = "customer/{CID}/loyaltyCard";               // id to set the backend error code in an context

        // contact backend and return json result
        return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
    }

    /*********************************
     *  post participation data
     ****************************** */
    public function postParticipationDataAction($key)
    {
        $eventID = $this->getApiKey($key);

        // set up backend call
        $csrfPhrase     = "participate_to_raffle";
        $dataObject     = new NewRaffleData();                                           // init object that holds request data
        $type           = "post";                                                        // set request type
        $resource       = "customer/" . $this->getCid() . "/campaignEvent/" . $eventID;  // backend resource, see wiki
        $context        = "customer/{CID}/campaignEvent/{EID}";                          // id to set the backend error code in an context

        if ( $this-> getCanParticipate($key) ) {
            // contact backend and return json result
            return $this->contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context );
        } else {
            $response   = new JsonResponse();
            $response->setData(array(
                'success' => false,
                'data' => array(
                    'message'   => "Entweder hast du schon teilgenommen, oder um den Gewinn kann gerade nicht gespielt werden."
                    )
                )
            );
            return $response;
        }

    }

    /*********************************
     *  serve prize image
     ****************************** */
    public function servePrizeImageAction( $key, $quality = 'lg' )
    {

        $debug = false;
        $logger     = $this->get('logger');
        $imageDefaultPath = $this->get('kernel')->locateResource('@Dept1McdBundle/Resources/public/images/prizes/404_IMAGE_NOT_FOUND_xs.png');

        if ( isset($key) ) {
            if ( preg_match('/[^A-Z0-9_]/', $key) ) {
                return new BinaryFileResponse( $imageDefaultPath );
            }
        }

        switch ( $quality ) {
            // check for allowed levels
            case "lg":
            case "sm":
            case "md":
            case "xs":
                break;
            default:
                // overwrite forebidden value with default
                $quality = "lg";
        }

        $imageName = $key . "_" . $quality . ".png";
        $imageBasePath = $this->get('kernel')->locateResource('@Dept1McdBundle/Resources/public/images/prizes');
        $imagePath = $imageBasePath . '/' . $imageName;

        if ( !file_exists( $imagePath ) ){
            $logger->error( "Serve prize image: Can't locate requested resource: " . $imagePath );
            $imagePath = $imageDefaultPath;
        }

        return new BinaryFileResponse( $imagePath );
    }

/* ------------------------------------------------------------------------
 *      Functions
 * ----------------------------------------------------------------------*/

    /***************************************
     *  prepare backend call
     ************************************* */
    public function contactBackend( $csrfPhrase, $dataObject, $type, $resource, $context )
    {
        $logger     = $this->get('logger');                         // init standard logger
        $request    = Request::createFromGlobals();                 // get data from ajax request

        // exit condition: show home if not an ajax request
        if( !$request->isXmlHttpRequest()) {
            $logger->error( "Non XmlHttpRequest detected: Redirect to: " . $this->redirectOnErrorUrl );
            return $this->redirect( $this->generateUrl( $this->redirectOnErrorUrl ) );
        }

        // exit condition: show error on wrong csrf token
        if ( !$this->checkCsrfToken( $this->get('form.csrf_provider') , $csrfPhrase , $request ) ) {
            return $this->buildCsrfErrorResponse();
        }

        // exit condition: show error on inavlid captcha
        if ( $this->haveToCheckCaptcha && !$this->checkCaptcha( $request ) ) {
            return $this->buildCaptureErrorResponse();
        }

        // validate user input
        $validator      = $this->get('validator');                  // init symfony validator
        $dataObject->setValuesFromRequest( $request );              // save user input in object
        $errors         = $validator->validate( $dataObject );      // validate user input, rules are set in config

        // exit condition: user input is not valid
        if ( count( $errors ) > 0 ) {
            $logger->error( "Fronend: Validation failed: " . serialize($errors) );
            return $this->buildValidationErrorResponse( $errors );
        }

        // contact backend
        $postData       = $dataObject->buildObjectForRequest();     // data that has to be send to backend
        $dummyResponse  = $dataObject->getDummyResponse();          // dummy data to work without backend response

        // send json response to caller
        return $this->callBackend( $type, $resource, $postData, $dummyResponse, $context );
    }

    /***************************************
     *  give data to iqone api
     ************************************* */
    public function callBackend( $type="get", $resource="isAlive", $postData="", $dummyResponse="", $context="default" )
    {
        $logger     = $this->get('logger');                         // init standard logger
        $response   = new JsonResponse();                           // prepare return object

        $messagePath = $this->get('kernel')->locateResource('@Dept1McdBundle/Resources/config/messages.yml');

        // init api connection
        $api        = $this->get('dept1_basic.IqOneApi');
        $apiUrl     = $this->getApiUrl();
        $sslParams  = $this->getSslParams();

        //init user feedback tool
        $feedback = $this->get('dept1_basic.ErrorFeedbackService');

        // debug switch
        if ( !$this->debug ) {
            // conect iqone api
            try {
                // choose request type
                switch ( $type ) {
                    case "get":
                        $api_result = $api->get( $apiUrl . $resource, $sslParams );
                        break;
                    case "put":
                        $api_result = $api->put( $apiUrl . $resource, $postData, $sslParams );
                        break;
                    case "post":
                        $api_result = $api->post( $apiUrl . $resource, $postData, $sslParams );
                        break;
                }
                // prepare response on success
                $response->setData(array(
                    'success'  => true
                ));
                // update user
                $user       = $this->get('dept1_basic.user');                         // init user
                $userData   = $user->getUserData();
                switch ($context) {
                    case "customers/login":
                        // save deeplink from session
                        $logger->debug( "Current deeplink is set: " . $user->getDeepLink() );
                        $deepLink = $user->getDeepLink();

                        $this->get('session')->invalidate();                    // new session
                        $user->setIsLoggedIn(true);                             // set status
                        $user->setUserData($api_result);                        // save response
                        $userData   = $user->getUserData();                     // get new user data
                        $logger->debug( "API Response: " . serialize(json_encode($api_result)) );

                        // if session holds link -> follow
                        if ( $deepLink ) {
                            $response->setData(array(
                                'success' => true,
                                'deeplink' => $user->getDeepLink()
                            ));
                        // if not, we can show some .. add
                        } else {
                            // if user has no card, show the card banner
                            if ( !isset( $userData->loyaltyCards ) ||
                                 ( isset( $userData->loyaltyCards ) && empty( $userData->loyaltyCards ) ) ) {
                                $response->setData(array(
                                    'success' => true,
                                    'deeplink' => $this->generateUrl('dept1_mcd_card_banner')
                                ));
                            }
                        }
                        break;
                    case "customers/passwordRequest":
                        break;
                    case "customer/{CID}/cancel":
                        $this->get('session')->invalidate();                    // new session
                        break;
                    case "customer/{CID}/password":
                    case "customer/{CID}/email":
                    case "customer/{CID}":
                    case "customer/{CID}/optin":
                    case "customer/{CID}/loyaltyCard":
                        $user->setUserData( $this->updateUserData( $this->getCid() ) );    // save response
                        break;
                }
            } catch (\Exception $e) {
                // prepare response on fail
                $errorCode = $e->getMessage();
                $httpCode  = $e->getCode();
                $logger->debug( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
                $userMessage = $feedback->getMessage( $messagePath, $errorCode, $httpCode, $context );
                $api_result = NULL;
                switch ($context) {
                    case "customers/passwordRequest":
                        $response->setData(array(
                            'success' => true
                        ));
                        break;
                    case "customer/{CID}/loyaltyCard":
                        if ( $errorCode == "YearlyLimitReached" ) {
                            $limitReached = true;
                        } else {
                            $limitReached = false;
                        }
                        $response->setData(array(
                            'success' => false,
                            'data'  => array(
                                'message'       => $userMessage,
                                'limitReached'  => $limitReached
                            )
                        ));
                       break;
                    default:
                        $response->setData(array(
                            'success' => false,
                            'data'  => array(
                                'message'   => $userMessage
                            )
                        ));
                }

            }
        } else {
            // prepare response for test
            $logger->debug( "run api debug mode" );
            // send dummy data
            if ( $this->simBackEnd ) {
                $logger->debug( "use dummy response (string): " . $dummyResponse );
                $dummyResponse = json_decode($dummyResponse);
                $logger->debug( "json decode error message: " . json_last_error_msg() );
                $response->setData(array(
                    'success'   => true,
                    'data'      => $dummyResponse
                    )
                );
                //update user on login
                if ( $context == "customers/login") {
                    $logger->debug( "login: save dummy data to user" );
                    $user = $this->get('dept1_basic.user'); // init user
                    $user->setIsLoggedIn(true);             // set status
                    $user->setUserData($dummyResponse);     // save sim response
                    $logger->debug( "This data was saved in user: " . serialize($user->getUserData()) );
                }
                //update user on cancel member ship
                if ( $context == "customer/cancel") {
                    $logger->debug( "cancel membership: save dummy data to user" );
                    $user = $this->get('dept1_basic.user');     // init user
                    $user->setIsLoggedIn(false);                // set status
                    $user->setUserData(NULL);                   // save sim response
                    $logger->debug( "This data was saved in user: " . serialize($user->getUserData()) );
                }
            // just send user input back
            } else {
                $response->setData(array(
                    'success'   => true,
                    'data'      => $postData
                ));
            }
        }
        return $response;
    }

    /***************************************
     *  get customer
     ************************************* */
    public function updateUserData( $CID )
    {
        $logger = $this->get('logger');

        // init api connection
        $api = $this->get('dept1_basic.IqOneApi');
        $apiUrl = $this->getApiUrl();
        $sslParams = $this->getSslParams();

        // conect iqone api
        try {
            $api_result = $api->get($apiUrl.'customer/'. $CID, $sslParams);
        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
            $httpCode  = $e->getCode();
            $logger->error( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
            $api_result = false;
        }
        return $api_result;
    }

    /***************************************
     *  get raffle data
     *  Return a list of all event identifiers (of a preconfigured set) the given
     *  customer is eligible to, that means the campaign is valid and the customer
     *  did not participate yet.*
     ************************************* */
    public function getRaffleData( $CID )
    {
        $logger = $this->get('logger');

        // init api connection
        $api = $this->get('dept1_basic.IqOneApi');
        $apiUrl = $this->getApiUrl();
        $sslParams = $this->getSslParams();

        // conect iqone api
        try {
            $api_result = $api->get($apiUrl.'customer/'. $CID . '/eligibleCampaignEvents', $sslParams);
        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
            $httpCode  = $e->getCode();
            $logger->error( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
            $api_result = false;
        }
        return $api_result;
    }

    /***************************************
     *  reedem reward
     ************************************* */
    public function postRedeemRewardDataAction( $reward )
    {
        $logger = $this->get('logger');
        $response = new JsonResponse();
        $user = $this->get('dept1_basic.user');
        $userData = $user->getUserData();

        $logger->debug(  $userData->id );
        $logger->debug(  $userData->email );

        $api_result = false;

        // reserve reward
        // init api connection
        $api = $this->get('dept1_basic.IqOneApi');
        $apiUrl = $this->getApiUrl();
        $sslParams = $this->getSslParams();

        // try to reserve premium
        try {
            $api_result = $api->post($apiUrl.'customer/'. $userData->id . '/redeem/PLAYSTATION', false, $sslParams);
            $logger->debug( "Backend returns transaction code: " . $api_result );
        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
            $httpCode  = $e->getCode();
            $logger->error( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
            $feedback = $this->get('dept1_basic.ErrorFeedbackService');
            $messagePath = $this->get('kernel')->locateResource('@Dept1McdBundle/Resources/config/messages.yml');
            $message = $feedback->getMessage( $messagePath, $errorCode, $httpCode, 'customer/{CID}/redeem/PLAYSTATION' );
        }

        // premium reserved
        if ( $api_result ) {
            // contact creata and request check out url
            $creataApi = $this->get('dept1_basic.CreataApi');
            try {
                $creataApi_result = $creataApi->startPayment( $api_result, $userData->id, $userData->email );
                $logger->debug( "Creata backend result: " . $creataApi_result->url );
            } catch (\Exception $e) {
                $errorCode = $e->getMessage();
                $httpCode  = $e->getCode();
                $logger->error( "Creata reports an Error: " . $errorCode . "(" . $httpCode .")" );
            }
            // got checkout url from creata
            if( isset($creataApi_result->url) ) {
                $response->setData(array(
                    'success' => true,
                    'url'     => $creataApi_result->url
                ));
            // checkout failed
            } else {
                $message = "Die Kaufabwicklung konnte nicht gestartet werden.";
                // delete reservation
                try {
                    $api_result = $api->delete($apiUrl.'customer/'. $userData->id . '/reward/'. $api_result, $sslParams);
                    $logger->debug( "Backend trying to remove reservation: " . $api_result );
                    $message .= " Die Punkte wurden wieder gut geschrieben.";
                } catch (\Exception $e) {
                    $errorCode = $e->getMessage();
                    $httpCode  = $e->getCode();
                    $logger->error( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
                    $message .= " Die Punkte konnten nicht wieder gut geschrieben werden.";
                }
                $response->setData(array(
                    'success' => false,
                    'message' => $message
                ));
            }
        // premium not reserved
        } else {
            $response->setData(array(
                'success' => false,
                'message' => $message
            ));
        }

        return $response;
    }

    /***************************************
     *  get already participated
     *  check if user can participated on an raffle
     ************************************* */
    public function getCanParticipate($key)
    {
        $logger         = $this->get('logger');
        $user           = $this->get('dept1_basic.user');
        $canParticipate = false;
        $apiKey         = $this->getApiKey($key);
        $userData       = $user->getUserData();
        $raffleData     = $this->getRaffleData( $userData->id );
        $inTime         = false;
        switch ($apiKey) {
            case "XMAS_RAFFLE_TOYOTA":
                $startTime  = "01-12-2016 00:00:00";
                $endTime    = "24-12-2016 23:59:59";
                break;
            case "XMAS_RAFFLE_REDBULL":
                $startTime  = "01-12-2016 00:00:00";
                $endTime    = "24-12-2016 23:59:59";
                break;
            case "XMAS_RAFFLE_SKI":
                $startTime  = "01-12-2016 00:00:00";
                $endTime    = "24-12-2016 23:59:59";
                break;
            case "XMAS_RAFFLE_SONY":
                $startTime  = "01-12-2016 00:00:00";
                $endTime    = "24-12-2016 23:59:59";
                break;
            case "XMAS_RAFFLE_AUA":
                $startTime  = "01-12-2016 00:00:00";
                $endTime    = "24-12-2016 23:59:59";
                break;
        }

        if (    !empty($raffleData)             &&
                is_array($raffleData)           &&
                in_array( $apiKey, $raffleData) &&
                $this->isOnline($startTime)     &&
                !$this->isOver($endTime) ) {
            $canParticipate = true;
        }
        return $canParticipate;
    }

    /***************************************
     *  translate key
     ************************************* */
    public function getApiKey($key)
    {
        $logger = $this->get('logger');

        $apiKey = "UNKNOWN";
        switch( $key ){
            case "toyota":
                $apiKey = "XMAS_RAFFLE_TOYOTA";
                break;
            case "redbull":
                $apiKey = "XMAS_RAFFLE_REDBULL";
                break;
            case "ski":
                $apiKey = "XMAS_RAFFLE_SKI";
                break;
            case "ps4":
                $apiKey = "XMAS_RAFFLE_SONY";
                break;
            case "flight":
                $apiKey = "XMAS_RAFFLE_AUA";
                break;
        }
        return $apiKey;
    }

    /***************************************
     *  check if given time is over
     ************************************* */
    public function isOver($endtime)
    {
        $logger = $this->get('logger');
        $user = $this->get('dept1_basic.user');
        $isOver = false;
        $currentTime   = $user->getCurrentTime();
        if ( $currentTime > strtotime($endtime) ) {
            $isOver = true;
        }
        return $isOver;
    }

    /***************************************
     *  check if given time not reached yet
     ************************************* */
    public function isOnline($starttime)
    {
        $logger = $this->get('logger');
        $user = $this->get('dept1_basic.user');
        $isOnline = false;
        $currentTime   = $user->getCurrentTime();
        $logger->debug("current time: " . $currentTime);
        $logger->debug("starttime: " . $currentTime);
        if ( $currentTime >= strtotime($starttime) ) {
            $isOnline = true;
        }
        $logger->debug("is online: " . ($isOnline)?"true":"false");
        return $isOnline;
    }

    /***************************************
     *  get loyalty point activities
     ************************************* */
    public function getLoyaltyPointActivities( $CID )
    {
        $logger = $this->get('logger');

        // init api connection
        $api = $this->get('dept1_basic.IqOneApi');
        $apiUrl = $this->getApiUrl();
        $sslParams = $this->getSslParams();

        // conect iqone api
        try {
            $api_result = $api->get($apiUrl.'customer/'. $CID . '/loyaltyPointActivities', $sslParams);
        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
            $httpCode  = $e->getCode();
            $logger->debug( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
            $api_result = false;
        }

        // debug output
        $debug_json = '{"loyaltyPointActivities":[{"id":147,"displayName":"Bestellung","dateTime":"2016-09-16 08:01:27","loyaltyPointActivityType":"PURCHASE","points":116,"address":null},{"id":143,"displayName":"Bestellung","dateTime":"2016-09-15 11:23:41","loyaltyPointActivityType":"PURCHASE","points":15,"address":null},{"id":128,"displayName":"Bestellung","dateTime":"2016-09-15 11:12:00","loyaltyPointActivityType":"PURCHASE","points":1,"address":null},{"id":127,"displayName":"Bestellung","dateTime":"2016-09-15 11:11:00","loyaltyPointActivityType":"PURCHASE","points":2,"address":null},{"id":126,"displayName":"Bestellung","dateTime":"2016-09-15 11:09:14","loyaltyPointActivityType":"PURCHASE","points":44,"address":null},{"id":125,"displayName":"Bestellung","dateTime":"2016-09-15 11:06:58","loyaltyPointActivityType":"PURCHASE","points":2,"address":null},{"id":123,"displayName":"Bestellung","dateTime":"2016-09-15 11:04:59","loyaltyPointActivityType":"PURCHASE","points":9,"address":null},{"id":74,"displayName":"Bestellung","dateTime":"2016-09-14 10:18:18","loyaltyPointActivityType":"PURCHASE","points":4,"address":null},{"id":73,"displayName":"Bestellung","dateTime":"2016-09-13 14:32:37","loyaltyPointActivityType":"PURCHASE","points":1,"address":{"phone":"","city":"test","street":"teststrasse","addendum":null,"streetNumber":null,"zip":"12345","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":63,"displayName":"Bestellung","dateTime":"2016-09-13 11:07:43","loyaltyPointActivityType":"PURCHASE","points":37,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":62,"displayName":"Bestellung","dateTime":"2016-09-13 11:06:43","loyaltyPointActivityType":"PURCHASE","points":10,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":61,"displayName":"Bestellung","dateTime":"2016-09-13 11:02:33","loyaltyPointActivityType":"PURCHASE","points":1,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":59,"displayName":"Bestellung","dateTime":"2016-09-13 11:00:46","loyaltyPointActivityType":"PURCHASE","points":12,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":58,"displayName":"Bestellung","dateTime":"2016-09-13 10:56:32","loyaltyPointActivityType":"PURCHASE","points":4,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":42,"displayName":"Bestellung","dateTime":"2016-09-09 14:30:37","loyaltyPointActivityType":"PURCHASE","points":4,"address":{"phone":"","city":"","street":"","addendum":null,"streetNumber":null,"zip":"","federalStateId":null,"countryCode":null,"fax":"","mobile":null}},{"id":35,"displayName":"Bestellung","dateTime":"2016-09-08 18:12:03","loyaltyPointActivityType":"PURCHASE","points":36,"address":null},{"id":23,"displayName":"Korrektur","dateTime":"2016-09-08 10:38:31","loyaltyPointActivityType":"CORRECTION","points":-10,"address":null},{"id":20,"displayName":"Registrierung","dateTime":"2016-09-08 10:33:49","loyaltyPointActivityType":"REGISTRATION","points":10,"address":null}],"expiryPoints":44,"expiryDate":"2016-09-15"}';

        if ( $this->debug ) {
            return json_decode($debug_json);
        } else {
            return $api_result;
        }
    }

    /*********************************
     *  get url from config
     ****************************** */
    public function getApiUrl()
    {
        if( $this->getSslParams() ) {
            # have ssl params
            $apiUrl = $this->container->getParameter( 'api_url_ssl' );
        } else {
            # no ssl params available
            $apiUrl = $this->container->getParameter( 'api_url' );
        }
        return $apiUrl;
    }

    /*********************************
     *  get cid
     ****************************** */
    public function getCid()
    {
        $logger = $this->get('logger');
        $user = $this->get('dept1_basic.user');
        $userData = $user->getUserData();
        return $userData->id;
    }


    /*********************************
     *  get ssl keys from config
     ****************************** */
    public function getSslParams()
    {
        if( $this->container->getParameter( 'is_ssl' ) ) {
            $sslParams = true;
/*             $sslParams = array(
                    'path' => Config::get('app.cert_path').Config::get('app.env').'/',
                    'curlCaInfo' => Config::get('app.curl_cainfo'),
                    'curlSslCert' => Config::get('app.curl_sslcert'),
                    'curlSslKey' => Config::get('app.curl_sslkey')
            ); */
        } else {
            $sslParams = NULL;
        }
        return $sslParams;
    }

    /*********************************
     * validate recaptcha string
     ****************************** */
    public function checkCaptcha( $request ) {
        $logger             = $this->get('logger');
        $apiUrl             = $this->container->getParameter( 'reCaptchaURL' );
        $secretKey          = $this->container->getParameter( 'reCaptchaKeySecure' );
        $captcha_response   = $request->request->get('captcha');
        try {
            $response = file_get_contents( $apiUrl . "?secret=". $secretKey . "&response=" . $captcha_response );
            $validation = json_decode($response);
        } catch (Exception $e) {
            $logger->error( 'Error on validation reCaptch string: ' . $e->getMessage() );
            $validation = json_decode('{"success":false}');
        }
        return $validation->success;
    }

    /*********************************
     * validate csrf token
     ****************************** */
    public function checkCsrfToken( $token, $tokenString, $request ) {
        $logger             = $this->get('logger');
        if ( $token->isCsrfTokenValid( $tokenString, $request->request->get('csrftoken') ) ) {
            return true;
        } else {
            $logger->debug( "Frontend reports an Error: csrf check failed. Api call failed." );
            return false;
        }
    }

    /*********************************
     *  prepare validation error message
     ****************************** */
    public function buildValidationErrorResponse( $errors )
    {
        // prepare response object
        $response = new JsonResponse();

        $validationResults = array();
        foreach ($errors as $error) {
            $validationResults[ $error->getPropertyPath() ] = $error->getMessage();
        }

        $response->setData(array(
            'success' => false,
            'data'  => array(
                'message'               => "Deine Eingaben sind noch nicht korrekt:",
                'validation_results'    => $validationResults
            )
        ));
        return $response;
    }

    /*********************************
     *  prepare capture error message
     ****************************** */
    public function buildCaptureErrorResponse()
    {
        $response = new JsonResponse();
        $response->setData(array(
            'success' => false,
            'data'  => array(
                'message'   => "Wir vermuten, du bist ein Roboter.",
                'code'      => 'captcha check failed'
            )
        ));
        return $response;
    }

    /*********************************
     *  prepare csrf error message
     ****************************** */
    public function buildCsrfErrorResponse()
    {
        $response = new JsonResponse();
        $response->setData(array(
            'success' => false,
            'data'  => array(
                'message'   => "Das Formular konnte nicht verarbeitet werden.",
                'code'      => 'csrf check failed'
            )
        ));
        return $response;
    }

    /*********************************
     *  check for login
     *  - check if user is logged in and
     *    return $okPage if true, else go to login
     ****************************** */
    public function checkForLogin($okPage, $pageParams = array() )
    {
        $logger = $this->get('logger');
        $user   = $this->get('dept1_basic.user');

        if ( $user->getIsLoggedIn() ) {
            return $this->render( $okPage, $pageParams );
        } else {
            // save deeplink
            $user->setDeepLink( $this->container->get('request_stack')->getCurrentRequest()->getUri() );
            $logger->debug( "Saved URL for deeplink handling: " . $user->getDeepLink() );
            return $this->redirect( $this->generateUrl('dept1_mcd_login') );
        }
    }

    /*********************************
     *  get user data from backend
     *  and refresh user object
     ****************************** */
    public function getAndRefreshUser()
    {
        $logger = $this->get('logger');
        $user   = $this->get('dept1_basic.user');

        // if user is logged in
        if ( $user->getIsLoggedIn() ) {
            // get user id
            $userData   = $user->getUserData();
            $logger->debug( "User is known ($userData->id), getNRefresh its data." );
            // get user data from backend and save it in user object
            $user->setUserData( $this->updateUserData( $userData->id ) );
        }

        return true;
    }

    /***************************************
     *  get reward data
     ************************************* */
    public function getRewards()
    {
        $logger = $this->get('logger');

        // init api connection
        $api = $this->get('dept1_basic.IqOneApi');
        $apiUrl = $this->getApiUrl();
        $sslParams = $this->getSslParams();

        // conect iqone api
        try {
            $api_result = $api->get($apiUrl.'rewards', $sslParams);
            $logger->debug( "Got rewards: " . serialize($api_result) );
        } catch (\Exception $e) {
            $errorCode = $e->getMessage();
            $httpCode  = $e->getCode();
            $logger->debug( "Backend reports an Error: " . $errorCode . "(" . $httpCode .")" );
            $api_result = false;
        }

        // debug output
        $debug_json = '[{ "id": 5500,"itemId": null,"code": null,"name": "Big Mac","nameKey": "REWARD_BIG_MAC","value": null,"url": "http://mcd16-test2.dept1.de/images/prizes/REWARD_BIG_MAC","loyaltyPoints": 30,"description": "1 Big Mac um 30 Ms","shortName": null,"amount": null,"validTillDate": null,"colourCode": "#01a3ce","additionalInfo": null},{"id": 5501,"itemId": null,"code": null,"name": "McChicken","nameKey": "REWARD_MCCHICKEN","value": null,"url": "http://mcd16-test2.dept1.de/images/prizes/REWARD_MCCHICKEN","loyaltyPoints": 30,"description": "1 McChicken um 30 Ms","shortName": null,"amount": null,"validTillDate": null,"colourCode": "#01a3ce","additionalInfo": null}]';

        if ( $this->debug ) {
            return json_decode($debug_json);
        } else {
            return $api_result;
        }
    }

    /*********************************
     *  manipulate current time
     ****************************** */
    public function setCurrentTimeAction($year, $month, $day, $hour, $minute, $second)
    {
        $user   = $this->get('dept1_basic.user');
        $user->setMyTime("$day-$month-$year $hour:$minute:$second");
        return new Response(
            '<html><body>Date set to: ' . "$day-$month-$year $hour:$minute:$second" . '</body></html>'
        );
    }
}