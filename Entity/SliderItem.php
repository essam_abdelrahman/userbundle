<?php

namespace DepartmentOne\CoreBundle\Entity;

class SliderItem
{
    private $id;
    private $itemId;
    private $code;
    private $name;
    private $nameKey;
    private $value;
    private $url;
    private $loyaltyPoints;
    private $description;
    private $shortName;
    private $amount;
    private $validTillDate;
    private $validFromDate;

    /**
     *
     * @param Array $item
     * @return SliderItem
     */
    public function __construct($item) {
        $this->id = $item->id;
        $this->itemId = $item->itemId;
        $this->code = $item->code;
        $this->name = $item->name;
        $this->nameKey = $item->nameKey;
        $this->value = $item->value;
        $this->url = $item->url;
        $this->loyaltyPoints = $item->loyaltyPoints;
        $this->description = $item->description;
        $this->shortName = $item->shortName;
        $this->amount = $item->amount;
        $this->validTillDate = $item->validTillDate;
        $this->validFromDate = $item->validFromDate;
    }

    public function getId() {
        return $this->id;
    }

    public function getItemId() {
        return $this->itemId;
    }

    public function getCode() {
        return $this->code;
    }

    public function getName() {
        return $this->name;
    }

    public function getNameKey() {
        return $this->nameKey;
    }

    public function getValue() {
        return $this->value;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getLoyaltyPoints() {
        return $this->loyaltyPoints;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getShortName() {
        return $this->shortName;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getValidTillDate() {
        return $this->validTillDate;
    }

    public function getValidFromDate() {
        return $this->validFromDate;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setItemId($itemId) {
        $this->itemId = $itemId;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setNameKey($nameKey) {
        $this->nameKey = $nameKey;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setLoyaltyPoints($loyaltyPoints) {
        $this->loyaltyPoints = $loyaltyPoints;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setShortName($shortName) {
        $this->shortName = $shortName;
    }

    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function setValidTillDate($validTillDate) {
        $this->validTillDate = $validTillDate;
    }

    public function setValidFromDate($validFromDate) {
        $this->validFromDate = $validFromDate;
    }


}
