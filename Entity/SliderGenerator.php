<?php

namespace DepartmentOne\CoreBundle\Entity;

use Dept1\BasicBundle\Entity\SliderItem;

class SliderGenerator
{
    
    /**
     *
     * @var Array $items Array of SliderItem items
     */
    private $items = array();
    
    
    public function __construct($items)
    {
        $error = false;
        if(!empty($items)) {
            foreach ($items as $item) {
                $sliderItem = new SliderItem($item);
                array_push($this->items, $sliderItem);
            }
        } else {
            $error = true;
        }
        
        if(!$error) {
            return $this->buildSlider();
        } else {
            return NULL;
        }
    }
    
    private function buildSlider()
    {
        return $this;
    }
}
