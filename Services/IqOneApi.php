<?php

namespace DepartmentOne\CoreBundle\Services;

/*
 * builds a request from params to contact iqone backend
 * returns backends response
 */
class IqOneApi
{

    protected   $response;
    private     $logger;
    private     $debug_message = array();

    public function __construct( $logger )
    {
        $this->logger = $logger;
        $this->writeToArray("error", "---- Frontend - IQoneAPI -------------");
    }

    public function get($param, $sslParams = NULL) {
        $this->request($param, 'get', false, $sslParams);
        return $this->getResponse();
    }

    public function post($param,$data, $sslParams = NULL) {
        $this->request($param,'post',$data, $sslParams);
        return $this->getResponse();
    }

    public function put($param,$data, $sslParams = NULL) {
        $this->request($param,'put',$data, $sslParams);
        return $this->getResponse();
    }

    public function delete($param, $sslParams = NULL) {
        $this->request($param,'delete', false, $sslParams);
        return $this->getResponse();
    }

    /*
     * send request with curl
     */
    private function request($param, $method = 'get', $data = false, $sslParams = NULL) {

        $this->writeToArray("error", "URL: "    . $param);
        $this->writeToArray("debug", "Method: " . $method);
        $this->writeToArray("debug", "SSL: "    . $sslParams);

        if( $sslParams ) {
            $options = array(
                CURLOPT_SSL_VERIFYPEER => true,
                CURLOPT_SSL_VERIFYHOST => 2,
                // server certificate
                CURLOPT_CAINFO => $sslParams['path'].$sslParams['curlCaInfo'],
                // client certificates
                CURLOPT_SSLCERT => $sslParams['path'].$sslParams['curlSslCert'],
                CURLOPT_SSLKEY => $sslParams['path'].$sslParams['curlSslKey'],

                CURLOPT_SSLVERSION => 'TLSv1',

                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            );
        } else {
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
            );
        }

        $url = $param;

        $handle = curl_init($url);

        switch($method) {
            case 'GET':
            case 'get':
                break;

            case 'POST':
            case 'post':
                array_push($options, curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'POST'));
                if($data && is_string($data)) {
                    array_push($options, curl_setopt($handle, CURLOPT_POSTFIELDS, $data));
                    $this->writeToArray("debug", "Data (not encoded): " . $data);
                } elseif ($data) {
                    array_push($options, curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data)));
                    $this->writeToArray("debug", "Data (json encoded): " . json_encode($data));
                }
                break;

            case 'PUT':
            case 'put':
                array_push($options, curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'PUT'));
                if($data) {
                    array_push($options, curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data)));
                    $this->writeToArray("debug", "Data (json encoded): " . json_encode($data));
                }
            break;

            case 'DELETE':
            case 'delete':
                array_push($options, curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'DELETE'));
            break;
        }

        curl_setopt_array( $handle, $options );
        $this->response = curl_exec($handle);

        if (curl_error($handle)) {
            $this->error = array('errno' => curl_errno($handle), 'error' => curl_error($handle));
            $curl_error_msg = "CURL Error ".curl_errno( $handle ) . " - " . curl_error( $handle );
            $this->writeToArray("error", $curl_error_msg);
            $this->logError();
            throw new \RuntimeException( $curl_error_msg );
        } else {
            $this->code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            $decodedResult = json_decode($this->response);

            if($this->code != 200 && isset($decodedResult->errorCode)) {
                $this->writeToArray("error", "IQone Error with Code: " . $decodedResult->errorCode . "(". $this->code . ") and message: " . $decodedResult->errorMessage);
                $this->logError();
                throw new \RuntimeException($decodedResult->errorCode, $this->code);
            } elseif($this->code != 200) {
                $this->writeToArray("error", "IQone Error: No Code available (". $this->code . ")");
                $this->logError();
                throw new \RuntimeException("Got no error code from backend.", $this->code);
            }
            $this->error = false;
        }
        // log success too
        //$this->logError();
        curl_close($handle);
    }

    /*
     * decode json response, if it was json encoded
     */
    private function getResponse() {
        $response = json_decode($this->response);
        switch(json_last_error()){
            case JSON_ERROR_SYNTAX:
                $response = $this->response;
                break;
        }
        return $response;
    }

    /*
     * adds level and message to message array
     */
    private function writeToArray( $logLevel, $errorMessage ) {
        $this->debug_message[] = array(
                                    "level"=>$logLevel,
                                    "message"=>$errorMessage
                                 );
    }

    /*
     * writes message to log
     */
    private function logError() {
        foreach ($this->debug_message as $entry) {
            if ( $entry["level"] == "debug" ) {
                $this->logger->addDebug( $entry["message"] );
            } else {
                $this->logger->addError( $entry["message"] );
            }
        }
    }
}
