
<?php

namespace DepartmentOne\CoreBundle\Services;

class CreataApi
{

    public function __construct()
    {
    }

    /********************************
     *  get stock count of ps4
     ***************************** */
    public function getPs4StockCount() {
        $apiUrl = "https://creata-payment.com/api/v1/mymcdat/product-count/";

        $handle = curl_init( $apiUrl );
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($handle, CURLOPT_POSTFIELDS, '{"product": "playstation"}' );
        $curl_response = curl_exec( $handle );

        // check for curl error
        if ( curl_error( $handle ) ) {
            $curl_error_msg = "CURL Error ".curl_errno( $handle ) . " - " . curl_error( $handle );
            //$this->logger->debug( "Creata API reports an curl error: " . $curl_error_msg );
            throw new \RuntimeException( $curl_error_msg );
        // no curl error
        } else {
            $this->code = curl_getinfo( $handle, CURLINFO_HTTP_CODE );
            // but check http code
            if( $this->code < 200 && $this->code >= 300 ) {
                //$this->logger->debug( "Creata API reports an http error: " . $this->code );
                throw new \RuntimeException( "Creata API answers other than http code 2xx: " . $this->code );
            }
        }

        //$this->logger->debug( "Creata response: " . $curl_response );
        curl_close($handle);
        //return json_decode( '{"success": true,"count": 0}' );
        return  json_decode( $curl_response );
    }

    /********************************
     *  start payment ps4
     ***************************** */
    public function startPayment( $transactionID, $customerID, $customerEmail ) {
        $apiUrl = "https://creata-payment.com/api/v1/mymcdat/start-order/";

        $requestBody = '{
                            "transaction":"'.$transactionID.'",
                            "user":{
                                "id":"'.$customerID.'",
                                "email": "'.$customerEmail.'"
                            },
                            "products":[
                                "playstation"
                            ]
                        }';
        $handle = curl_init( $apiUrl );
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($handle, CURLOPT_POSTFIELDS, $requestBody );
        $curl_response = curl_exec( $handle );

        // check for curl error
        if ( curl_error( $handle ) ) {
            $curl_error_msg = "CURL Error ".curl_errno( $handle ) . " - " . curl_error( $handle );
            //$this->logger->debug( "Creata API reports an curl error: " . $curl_error_msg );
            throw new \RuntimeException( $curl_error_msg );
        // no curl error
        } else {
            $this->code = curl_getinfo( $handle, CURLINFO_HTTP_CODE );
            // but check http code
            if( $this->code < 200 && $this->code >= 300 ) {
                //$this->logger->debug( "Creata API reports an http error: " . $this->code );
                throw new \RuntimeException( "Creata API answers other than http code 2xx: " . $this->code );
            }
        }
        curl_close($handle);
        return  json_decode( $curl_response );
    }
}