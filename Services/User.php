<?php

namespace DepartmentOne\CoreBundle\Services;

class User
{
    private $logger;
    private $isLoggedIn     = false;    // login status
    private $userData;                  // user data from backend
    private $session;                   // link to session object
    private $userStatus     = "basic";  // default loyalty status
    private $deepLink       = false;    // or url to open after login
    private $manipulateTime = false;
    private $currentTime    = "";

    /*
     * init user, load user data from session
     */
    public function __construct($logger, $session, $manipulate_time, $my_time)
    {
        $this->session          = $session;         // set link to session
        $this->logger           = $logger;
        $this->manipulateTime   = $manipulate_time;
        $this->currentTime      = strtotime($my_time);

        // override current time with given value
        if ( $this->manipulateTime ) {
            // get a customized time from session
            if ( !empty($session->get('myTime')) ) {
                $this->currentTime = $session->get('myTime');
            // or use defaults from settings
            } else {
                // no setting is given
                if ( empty($this->currentTime) ) {
                    // use server time (url modification is still possible)
                    $this->currentTime = time();
                }
            }
        // set current time to server time
        } else {
            // TODO: is server time equal to local time?
            $this->currentTime = time();
        }
        $this->logger->debug("Current time: " . date("d-m-Y H:i:s", $this->currentTime) );

        // login status is saved in session
        if ( $session->get('isLoggedIn') && $session->get('userData') && !empty($session->get('userData'))  ) {
            // save status in user
            $this->isLoggedIn = $session->get('isLoggedIn');
        }

        // auth code is saved in session
        if ( !empty($session->get('userAuthCode'))  ) {
            // save auth code in user
            $this->userAuthcode = $session->get('userAuthCode');
        }

        // deepLink is saved in session
        if ( !empty($session->get('deepLink'))  ) {
            // save deepLink in user
            $this->deepLink = $session->get('deepLink');
        }

        // user data are saved in session
        if ($session->get('userData')) {
            // save user data in usr
            $this->userData = $session->get('userData');

            // save loyalty status user status
            if ( isset( $this->userData->customerStatus->loyaltyStatus ) ) {
            switch ( $this->userData->customerStatus->loyaltyStatus ) {
                case "GOLD":
                    $this->userStatus = "gold";
                    break;
                case "PLATINUM":
                    $this->userStatus = "platinum";
                    break;
                }
            }
        }
    }

    /*
     * set login status and save status in session
     */
    public function setIsLoggedIn($isLoggedIn) {
        $this->isLoggedIn = $isLoggedIn;
        $this->session->set('isLoggedIn', $isLoggedIn);
    }

    /*
     * set auth code for user
     */
    public function setAuthCode( $authCode ) {
        $this->userAuthCode = $authCode;
        $this->session->set( 'userAuthCode', $authCode );
    }

    /*
     * set user data and save user data in session
     */
    public function setUserData($userData) {
        $this->logger->debug("Write user data: " . serialize($userData) );
        $this->userData = $userData;
        $this->session->set('userData', $userData);
    }

    /*
     * set deeplink
     */
    public function setDeepLink( $deepLink ) {
        $this->deepLink = $deepLink;
        $this->session->set( 'deepLink', $deepLink );
    }

    /*
     * set myTime
     */
    public function setMyTime( $myTime ) {
        $this->session->set( 'myTime', strtotime( $myTime ) );
    }

    /*
     * get login status from user
     */
    public function getIsLoggedIn() {
        return $this->isLoggedIn;
    }

    /*
     * get auth code from user
     */
    public function getAuthCode() {
        return $this->userAuthcode;
    }

    /*
     * get user data from user
     */
    public function getUserData() {
        return $this->userData;
    }

    /*
     * get user status from user
     */
    public function getUserStatus() {
        return $this->userStatus;
    }

    /*
     * get deeplink
     */
    public function getDeepLink() {
        return $this->deepLink;
    }

    /*
     * get current time
     */
    public function getCurrentTime() {
        return $this->currentTime;
    }
}
