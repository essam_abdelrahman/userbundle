<?php

namespace DepartmentOne\CoreBundle\Services;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
* ErrorFeedbackService
*
* Translates a backend error code into user readable text.
* Text is configured by an array. Perhaps editable by PMs in that way.
*
* Class contains some other error code tools:
* - get livesign
* - get error code and http code
*
* @author     tilo.neumann@departmentone.com
*/
class ErrorFeedbackService
{
    private $logger;
    private $userMessage = "Unbekannter Fehlercode.";
    private $messageLib;


    /**
     * __construct
     *
     * Get logger from service config.
     *
     * @param object $logger    symfony monolog logger object
     */
    public function __construct( $logger ) {
        $this->logger = $logger;
    }


    /**
     * isAlive
     *
     * Get a livesign from this service.
     *
     * @return boolean
     */
    public function isAlive() {
        return true;
    }

    /**
     * getCodes
     *
     * Transform both response codes from iqone api into a string.
     *
     * @param type $errorCode   error code from backend, like: InternalError
     * @param type $httpCode    http code from backend, like: 500
     *
     * @return string
     */
    public function getCodes( $errorCode, $httpCode ) {
        $codeMessage = $errorCode . "(" . $httpCode . ")";
        return $codeMessage;
    }

    /**
     * getMessage
     *
     * Transforms an error code from backend response into something user
     * readable. All "translations" are done in config file ($messagePath).
     *
     * Each error code can have an context code. AccessDenied can mean something
     * other and needs an other error message, if user wants to login or
     * change a email.
     *
     * @param string $messagePath path to message file
     * @param string $errorCode   error code from backend, like: InternalError
     * @param string $httpCode    http code from backend, like: 500
     * @param string $resource    context key, set by calling controller
     *
     * @return string
     */
    public function getMessage( $messagePath, $errorCode, $httpCode, $resource ) {

        try {
            $this->logger->addDebug( "Load error feedback from file: " . $messagePath );
            $this->messageLib = Yaml::parse(file_get_contents( $messagePath ));
        } catch (ParseException $e) {
            $this->logger->addDebug( "Unable to parse the YAML file: " .  $messagePath . " with error " . $e->getMessage() );
            // return default message
            return $this->userMessage;
        }

        // message for given resource is set
        if ( isset($resource)                                                                   &&
             array_key_exists( $httpCode, $this->messageLib[$resource] )                        &&
             array_key_exists( $errorCode, $this->messageLib[$resource][$httpCode] )            &&
             array_key_exists( 'message', $this->messageLib[$resource][$httpCode][$errorCode] ) &&
             !empty( $this->messageLib[$resource][$httpCode][$errorCode]['message'] )             ) {

            $this->logger->addError( "Generate error message for user: " . $resource . " - " . $errorCode . " - " . $httpCode . " - " . $this->messageLib[$resource][$httpCode][$errorCode]['message'] );
            $this->userMessage = $this->messageLib[$resource][$httpCode][$errorCode]['message'];

        // no message found
        } else {
            // look for default message
            if ( array_key_exists( $httpCode, $this->messageLib['default'] ) &&
                 array_key_exists( $errorCode, $this->messageLib['default'][$httpCode] ) ) {

                $this->userMessage = $this->messageLib['default'][$httpCode][$errorCode]['message'];
                $this->logger->addError( "Context was not found. Using default message for user: " . $errorCode . " (" . $httpCode . "):" . $this->userMessage );

            // unknown error code
            } else {
                $this->logger->addError( "Can't generate error message for user: " . $errorCode . " (" . $httpCode . "):" . $this->userMessage );
            }
        }

        // return user readable error message
        return $this->userMessage;
    }

} // end class

