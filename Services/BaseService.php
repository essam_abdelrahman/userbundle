<?php

namespace DepartmentOne\CoreBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;

class BaseService
{
    public $projectTitle = "";
    public $promoStart      = false;
    public $promoEnd        = false;
    public $isLive          = false;
    public $isOver          = false;
    public $screenLocked    = false;
    public $lockedClass     = "unlocked";
    public $reCaptchaKey    = "";
    public $userStatus      = "basic";
    public $isLoggedIn      = false;
    public $loginClass      = "logged_out";
    public $customerData    = "";
    public $yesterBirthDay  = "";
    public $logger           = "";
    public $fileLocator      = "";
    public $cssLastChange    = "";
    public $currentTime      = "";

    //TODO this should be replaced with one object only contained all of those variables.
    public function __construct(
                            $project_name,
                            $start_of_promo,
                            $end_of_promo,
                            $project_locked,
                            $locked_why,
                            $user,
                            $reCaptchaKey,
                            $logger,
                            $file_locator,
                            $rootDir
            )
    {
        $this->projectTitle     = $project_name;
        $this->promoStart       = $start_of_promo;
        $this->promoEnd         = $end_of_promo;
        $this->screenLocked     = $project_locked;
        $this->lockedClass      = $locked_why;
        $this->reCaptchaKey     = $reCaptchaKey;
        $this->logger           = $logger;
        $this->fileLocator      = $file_locator;

        $this->currentTime = $user->getCurrentTime();

        if ( $user->getIsLoggedIn() ) {
            $this->isLoggedIn = true;
            $this->loginClass = "logged_in";
        }

        if ( $user->getUserStatus() ) {
            $this->userStatus = $user->getUserStatus();
        }

        if ( $user->getUserData() ) {
            $this->customerData = $user->getUserData();
        }

        $this->yesterBirthDay = date("Ymd", strtotime("-14 years"));

        $filename = $rootDir . '/../web/css/myStyles.css';
        if (file_exists($filename)) {
            $this->cssLastChange =  date ("Ymd-His", filemtime($filename));
        }

        if (  strtotime($this->promoStart) <= $this->currentTime && $this->currentTime <= strtotime($this->promoEnd) ) {
            $this->isLive = true;
        } else {
            // promo is not live, why?
            if ( $this->currentTime >= strtotime($this->promoEnd) ) {
                $this->isOver = true;
            }
        }
    }
    public function getIsLive(){
        return $this->isLive;
    }
    public function getIsOver(){
        return $this->isOver;
    }
    public function getPromoStart(){
        return $this->promoStart;
    }
    public function getPromoEnd(){
        return $this->promoEnd;
    }
    public function getTitle(){
        return $this->projectTitle;
    }
    public function getScreenLocked(){
        return $this->screenLocked;
    }
    public function getLockedClass(){
        return $this->lockedClass;
    }
    public function getIsLoggedIn(){
        return $this->isLoggedIn;
    }
    public function getReCaptchaKey(){
        return $this->reCaptchaKey;
    }
    public function getUserStatus(){
        return $this->userStatus;
    }
    public function getLoginClass(){
        return $this->loginClass;
    }
    public function getCustomerData(){
        return $this->customerData;
    }
    public function getYesterBirthDay(){
        return $this->yesterBirthDay;
    }
    public function getCssLastChange(){
        return $this->cssLastChange;
    }
    public function getCurrentTime(){
        return $this->currentTime;
    }
}
